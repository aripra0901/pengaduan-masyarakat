<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pengaduan;
use App\Models\Pengaduan as ModelsPengaduan;
use Illuminate\Support\Facades\Auth;
use Termwind\Components\Dd;
use PDF;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('pengaduan')
        ->orderBy('created_at', 'desc')
       ->get();
  
        return view ('daftarLaporan', compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request )
    {
        
    }

    public function dataTable()
    {
       
        $data = DB::table('pengaduan')
       
        ->get();
        
        
        return view ('daftarLaporan', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('tanggapan')
        ->insert([
            'id_pengaduan' => $request->id_pengaduan,
            'tgl_tanggapan' => $request->tgl_tanggapan,
            'tanggapan' => $request->tanggapan,
            'id_petugas' => $request->id_petugas
        ]);
        return redirect()->route('daftarLaporan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('tanggapan', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
            $data = DB::table('pengaduan')
            ->where('id_pengaduan','=', $id)
                    ->update(['status' => 'proses']);

            //redirect to index
            return redirect()->route('daftarLaporan');
    }

     public function selesai($id)
     {
         $data = DB::table('pengaduan')
         ->where('id_pengaduan','=', $id)
                 ->update(['status' => 'selesai']);

         //redirect to index
         return redirect()->route('daftarLaporan');
     }

    public function destroy($id)
    {
        //
    }
    public function createPDF() {
        // retreive all records from db
        $data = Pengaduan::all();
        // share data to view
        $pdf = PDF::loadView('daftarLaporan.cetakpdf', ['daftarLaporan' => $data]);
        return $pdf->stream();
      }
}
