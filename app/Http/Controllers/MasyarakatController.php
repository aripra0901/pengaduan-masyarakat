<?php

namespace App\Http\Controllers;

use App\Models\Masyarakat;
use App\Models\User;
use App\Models\Pengaduan;
use App\Models\Admin;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MasyarakatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // menyimpan data file yang diupload ke variabel $file
        $validatedData = $request->validate([
            'foto' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
           ]);
    
           $name = $request->foto->getClientOriginalName();
           $path = $request->foto->store('public/storage/images');

        $data = Pengaduan::create([
            'foto' => $path,
            'isi_laporan' => $request->isi_laporan,
            'tgl_tanggapan' => $request->tgl_tanggapan,
            'nik' => Auth::user()->nik,
            'nama_pengadu' => Auth::user()->name,

        ]);
        return redirect()->route('dashboard');
    }

public function index()
    {
        $data = DB::table('pengaduan')
        ->get() ->where('nama_pengadu', '=' , Auth::user()->name);

        $tanggapan = Auth::id();
        $datatanggapan = DB::table('tanggapan')
        ->where('id_tanggapan', '=' , $tanggapan)->get();
        

        // dd($datatanggapan, $tanggapan);

        return view ('riwayatLaporan', compact('data','datatanggapan'));

    }
    public function tanggapan()
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function edit(Masyarakat $masyarakat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Masyarakat $masyarakat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Masyarakat  $masyarakat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Masyarakat $masyarakat)
    {
        //
    }
}
