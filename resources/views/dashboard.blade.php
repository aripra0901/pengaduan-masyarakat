<x-app-layout class="">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Selamat Datang di dashboard Laporan Masyarakat Desa Komba') }}
        </h2>
    </x-slot>
    @if(Auth::user()->role === 'administrator')
    <div class="container">
        <div class="card text-center mt-3 shadow">
            <div class="card-body bg-warning">
              <img src="../../assets/img/lapor.svg" class="w-50 float-start" alt="">
              <span class="card-title fs-2 fw-bold text-white d-flex mt-5 pt-4">Halo {{Auth::user()->name}}</span>
              <span class="card-title fs-3 fw-bold align-right text-white d-flex">Mari lihat laporan Masyarakat</span>
              <span class="card-title fs-3 fw-bold align-right text-white d-flex">Akhir akhir ini</span>
              
            </div>
           
            <a href="daftarLaporan" class="btn btn-sm btn-success ">Lihat Laporan</a>
          </div>
    </div>
    @endif
    @if(Auth::user()->role === 'petugas')
    <div class="container">
      <div class="card text-center mt-3 shadow">
          <div class="card-body bg-warning">
            <img src="../../assets/img/lapor.svg" class="w-50 float-start" alt="">
            <span class="card-title fs-2 fw-bold text-white d-flex mt-5 pt-4">Halo {{Auth::user()->name}}</span>
            <span class="card-title fs-3 fw-bold align-right text-white d-flex">Mari lihat laporan Masyarakat</span>
            <span class="card-title fs-3 fw-bold align-right text-white d-flex">Akhir akhir ini</span>
            
          </div>
         
          <a href="daftarLaporan" class="btn btn-sm btn-success ">Lihat Laporan</a>
        </div>
  </div>
  @endif
  @if(Auth::user()->role === 'masyarakat')
  <div class="container">
    <div class="card text-center mt-3 shadow">
        <div class="card-body bg-warning">
          <img src="../../assets/img/buat.svg" class="w-25 float-start" alt="">
          <span class="card-title fs-2 fw-bold text-white d-flex mt-4 pt-4">Halo {{Auth::user()->name}}</span>
          <span class="card-title fs-3 fw-bold align-right text-white d-flex">Ayo buat laporan masalah di sekitarmu </span>
          <span class="card-title fs-3 fw-bold align-right text-white d-flex">Akhir akhir ini</span>
          
        </div>
       
        <a href="buatLaporan" class="btn btn-sm btn-success ">Buat Laporan</a>
      </div>
</div>
@endif

   
</x-app-layout>
