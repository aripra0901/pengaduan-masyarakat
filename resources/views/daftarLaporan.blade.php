<x-app-layout class="">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Selamat Datang di dashboard ') }}
        </h2>
    </x-slot>

    <div class="container">
     
      
      <table class="table bg-light mt-5 rounded shadow-lg table-light table-striped">
  <thead>
    <tr>
      <th scope="col">Tanggal </th>
      <th scope="col">Status</th>
      <th scope="col">Pengadu</th>
      <th scope="col">Isi Aduan</th>
      <th scope="col">Foto</th>
      <th scope="col"></th>
      
    </tr>
  </thead>
  
  <tbody>
    
      @foreach ($data as $item)
          <tr>
            <td class="">{{$item->tgl_pengaduan}}</td>        
            <td>
              @if($item->status === 'tunggu')
                <div class="btn-group"> 
                  <div class="btn btn-sm btn-outline-primary rounded-pill text-white fw-bold bg-danger" data-bs-toggle="dropdown" aria-expanded="false">{{$item->status}}</div>
                  </div>
             @endif
              @if($item->status === 'proses')
                <div class="btn-group"> 
                  <div class="btn btn-sm btn-outline-primary rounded-pill text-white fw-bold bg-info" data-bs-toggle="dropdown" aria-expanded="false">{{$item->status}}</div>
                  </div>
             @endif
              @if($item->status === 'selesai')
                <div class="btn-group"> 
                  <div class="btn btn-sm btn-outline-primary rounded-pill text-white fw-bold bg-success" data-bs-toggle="dropdown" aria-expanded="false">{{$item->status}}</div>
                  </div>
             @endif
            </td>
            <td>{{$item->nama_pengadu}}</td>
            <td>{{$item->isi_laporan}}</td>
            <td>
              @if ($item->foto)
              <img src="{{ Storage::url($item->foto) }}" height="75" width="75" alt="" />
              @endif
             </td>
            <td>
              <a href="{{url('tanggapan/'. $item->id_pengaduan)}}" class="btn btn-sm btn-success w-100 "  > Berikan Tanggapan  </a href="{{url('tanggapan/'. $item->id_pengaduan)}}">
                <div class="row mt-1">
                  <div class="col">
                    <form action="{{ url('proses/'. $item->id_pengaduan) }}" method="POST">
                      @csrf
                      @method('PUT')
                    <button type="submit" class="btn btn-sm btn-info text-warning fw-bold w-100" as="h6">Proses</button> 
                    </form>

                  </div>
                  <div class="col">
                  <form action="{{ url('selesai/'.  $item->id_pengaduan) }}" method="POST">
                    @csrf
                    @method('PUT')
                  <button class="btn w-25 btn-sm btn-warning text-success fw-bold w-100" type="submit" as="h6">Selesai</button>
                  </form>
                  </div>
                </div>
            </td>        
          </tr>
      @endforeach
     
    
  </tbody>
  </table>  

  @if(Auth::user()->role === 'administrator')
  <a class="btn btn-outline-success w-100" href="{{ URL::to('/admin/pdf') }}">Export to PDF</a>
  @endif

 
    
</x-app-layout>

<div class="modal fade" id="tanggapanmodal" tabindex="-1" aria-labelledby="tanggapanmodalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content border-0">
      <div class="modal-header bg-warning border-0">
        <h5 class="modal-title fw-bold " id="tanggapanmodalLabel">Beri Tanggapan</h5>
        <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form class="" >
    <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Nama Petugas</label>
    <input type="hidden" class="form-control rounded border-secondary border" value={{Auth::user()->id}} name="id" aria-describedby="emailHelp" disabled>
    <input type="text" class="form-control rounded border-secondary border" value={{Auth::user()->name}} name="name" aria-describedby="emailHelp" disabled>
    <div id="emailHelp" class="form-text"></div>
  

  <div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Berikan Tanggapan</label>
  <textarea class="form-control rounded border-secondary border" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>

  
  <button type="submit" class="btn btn-sm btn-warning mb-3 w-100">Submit</button>
</form>


      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary text-dark" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary text-dark">Save changes</button>
      </div> -->
    </div>
  </div>
</div>


