@php
    use Carbon\Carbon;
@endphp

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<div>
    <center>
        <h3 class="fw-bold fs-5">Daftar Laporan Masyarakat</h3>
    </center>
</div>

  <table border="1" cellspacing="1" cellpadding="5" style="width: 100%;">
    <thead>
        <tr class="table-active">
          <th scope="col">Tanggal </th>
          <th scope="col">Status</th>
          <th scope="col">Pengadu</th>
          <th scope="col">Isi Aduan</th>
        </tr>
      </thead>

    <tbody>
        @foreach ($daftarLaporan as $item)
        <tr class="table-active">
          <td class="">{{$item->tgl_pengaduan}}</td>q
          <td>
            <div class="btn-group"> 
              @if($item->status === 'tunggu')
            <div class="text-danger" data-bs-toggle="dropdown" aria-expanded="false">{{$item->status}}</div>
              @endif
              @if($item->status === 'proses')
              <div class="text-primary" data-bs-toggle="dropdown" aria-expanded="false">{{$item->status}}</div>
                @endif
                @if($item->status === 'selesai')
                <div class="text-success" data-bs-toggle="dropdown" aria-expanded="false">{{$item->status}}</div>
                  @endif
            </div> 
          </td>
          <td>{{$item->nama_pengadu}}</td>
          <td>{{$item->isi_laporan}}</td>
        </tr>
    @endforeach
   
    </tbody>

</table>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>