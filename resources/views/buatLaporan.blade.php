<x-app-layout class="">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          Selamat Datang Di Dashboard Pengaduan Masyarakat Desa Komba
        </h2>
    </x-slot>

    <div class="container rounded bg-light  shadow-lg mt-3 mb-3" >
      
<form class="mt-5" method="POST" action="{{ route('masyarakat.store') }}" enctype="multipart/form-data">
  @csrf
  @method('POST')
    <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Nama Pengadu</label>
    <input type="text" class="form-control rounded border-secondary border" name="nama_pengadu" value={{Auth::user()->name}} id="exampleInputEmail1" aria-describedby="emailHelp" disabled>
    {{-- <input type="hidden" class="form-control rounded border-secondary border" name="nik" value={{Auth::user()->nik}} id="exampleInputEmail1" aria-describedby="emailHelp" disabled> --}}
    <div id="emailHelp" class="form-text"></div>
  

  <div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Jelaskan Masalah</label>
  <textarea class="form-control rounded border-secondary border" name="isi_laporan" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>

  <div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Tanggal</label>
  <input type="date" class="form-control rounded border-secondary border" name="tgl_pengaduan" id="exampleFormControlTextarea1" rows="3">
  </div>
  
  <div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Foto Bukti Masalah</label>
  <input type="file" class="" name="foto" id="exampleFormControlTextarea1" rows="3">
  </div>

  
  <button type="submit" class="btn btn-sm btn-warning mb-3 w-100">Submit</button>
</form>

</div>
    
</x-app-layout>
