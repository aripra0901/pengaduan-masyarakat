<x-app-layout class="">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          Selamat Datang Di Dashboard
        </h2>
    </x-slot>

    <div class="container rounded bg-light  shadow-lg mt-3 mb-3" >
      
<form class="mt-5" method="POST" action="{{ route('tanggapan.store') }}" enctype="multipart/form-data">
  @csrf
  @method('POST')

  <input type="hidden" class="form-control rounded border-secondary border" name="id_tanggapan"  >
  <input type="hidden" class="form-control rounded border-secondary border" name="id_pengaduan" value={{$id}} id="exampleInputEmail1" >
  <input type="hidden" class="form-control rounded border-secondary border" name="id_petugas" value={{Auth::user()->id}} >

    <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Nama Petugas</label>
    <input type="text" class="form-control rounded border-secondary border" name="nama_pengadu" value={{Auth::user()->name}} id="exampleInputEmail1" aria-describedby="emailHelp" disabled>
    {{-- <input type="hidden" class="form-control rounded border-secondary border" name="nik" value={{Auth::user()->nik}} id="exampleInputEmail1" aria-describedby="emailHelp" disabled> --}}
    <div id="emailHelp" class="form-text"></div>

    <input type="date" class="form-control rounded border-secondary border" name="tgl_tanggapan" id="exampleFormControlTextarea1" rows="3">
  <div class="mb-3">
  <label for="exampleFormControlTextarea1" class="form-label">Berikan Tanggapan</label>
  <textarea class="form-control rounded border-secondary border" name="tanggapan" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>

  <button type="submit" class="btn btn-sm btn-warning mb-3 w-100">Submit</button>
</form>

</div>
    
</x-app-layout>
