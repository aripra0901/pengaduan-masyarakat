<x-app-layout class="">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          Selamat Datang Di Dashboard
        </h2>
    </x-slot>

    <div class="container rounded bg-light  shadow-lg mt-3 mb-3" >
      

</div>

<div class="container">
  <div class="row">
    @foreach ($data as $item)
        
    <div class="card w-75 mt-4 bg-warning border-0 shadow-lg">
      <div class="">
        <span class="fw-bold ms-3 ">Laporanmu:</span>
        <br/>
        <span class="fw-normal ms-3 fs-6 me-3 text-white">"{{$item->isi_laporan}}"</span>
      </div>
      <div class="mt-2">
        <span class="fw-bold ms-3">Tanggapan Petugas:</span>
        <br/>
        @foreach ($datatanggapan as $value)
        <span class="fw-normal ms-3 fs-6  me-3 text-white">"{{$value->tanggapan}}"</span>
        @endforeach
      </div>
      <div class="mt-2"> 
        <span class="fw-bold ms-3 float-start">Status Laporan:</span>
        @if($item->status === 'tunggu')
        <span class="float-end rounded-pill badge bg-danger"> {{$item->status}} </span>
        @endif
        @if($item->status === 'proses')
        <span class="float-end rounded-pill badge bg-primary"> {{$item->status}} </span>
        @endif
        @if($item->status === 'selesai')
        <span class="float-end rounded-pill badge bg-success"> {{$item->status}} </span>
        @endif
      </div>
    </div>
    @endforeach
    </div>  
  </div>
</div>
    
</x-app-layout>
