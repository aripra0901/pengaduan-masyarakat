<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MasyarakatController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function () {
    return view('dashboard');

})->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/daftarLaporan', [AdminController::class, 'index'])->middleware(['auth', 'verified'])->name('daftarLaporan');

Route::get('/buatLaporan', function () {
    return view('buatLaporan');
})->middleware(['auth', 'verified'])->name('buatLaporan');

Route::resource('/tanggapan', AdminController::class)->middleware(['auth', 'verified']);

Route::get('/riwayatLaporan', [MasyarakatController::class, 'index',])->middleware(['auth', 'verified'])->name('riwayatLaporan');


// Route::middleware(['auth','verified', 'checkRole:Administrator'])->group(function () {
//     // Route::post('/buatLaporan', [MasyarakatController::class, 'store'])->name('masyarakat.store');
//     Route::resource('masyarakat', MasyarakatController::class);
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

Route::middleware('auth')->group (function () {
    Route::get('/admin/pdf', [AdminController::class, 'createPDF']);

    // Route::post('/buatLaporan', [MasyarakatController::class, 'store'])->name('masyarakat.store');
    Route::resource('masyarakat', MasyarakatController::class);
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::put('proses/{id}', [AdminController::class, 'update'])->name('proses');
    Route::put('selesai/{id}', [AdminController::class, 'selesai'])->name('selesai');
});

require __DIR__.'/auth.php';
